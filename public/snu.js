// JavaScript for main HTML elements and navigation sidebar
//
// normally these routines do not need editing or customization,
// as long as the HTML elements and classes in index.html
// are not renamed.
//
// this code is executed once index.html has been loaded.
//
// HTML element id attributes start with "snu_",
// and classList elements start with "snu-".
//
// Jeffrey Biesiadecki, 1/6/2018

// initialize main elements and the navigation sidebar
{
  let elementList;
  let element;

  // create action function to open the navigation sidebar
  let snuOpen = function() {
    // show the snu_sidebar and close button, hide the open button
    document.getElementById('snu_sidebar').style.display = 'block';
    document.getElementById('snu_open_button').style.display = 'none';
    document.getElementById('snu_close_button').style.display = 'block';

    // fit the content to the right of the sidebar
    document.getElementById('snu_main').style.left =
      document.getElementById('snu_sidebar').style.width;
  }

  // create action function to close the navigation sidebar
  let snuClose = function() {
    // hide the snu_sidebar and close button, show the open button
    document.getElementById('snu_sidebar').style.display = 'none';
    document.getElementById('snu_open_button').style.display = 'block';
    document.getElementById('snu_close_button').style.display = 'none';

    // fit the content to the edge of the screen
    document.getElementById('snu_main').style.left = '0';
  }

  // create action function to select active sidebar button
  let snuActivate = function() {
    let idClicked = this.id;
    let elementList;

    // activate the clicked button, and make sure all others are inactive
    elementList = document.getElementsByClassName('snu-sidebar-button');
    for (let i = 0; i < elementList.length; i++) {
      let element = elementList[i];

      if (element.id === idClicked) {
        element.classList.add('snu-active');
      } else {
        element.classList.remove('snu-active');
      }
    }

    // also, make sure all accordions are marked as inactive
    elementList = document.getElementsByClassName('snu-sidebar-accordion');
    for (let i = 0; i < elementList.length; i++) {
      let element = elementList[i];

      element.classList.remove('snu-active');
    }
  }

  // create action function to open/close an accordion list
  // in the navigation sidebar
  let snuAccordion = function() {
    let idClicked = this.id;
    let elementDiv = document.getElementById(idClicked + '_div');
    let elementCaret = document.getElementById(idClicked + '_caret');

    if (elementDiv.classList.contains('w3-hide')) {
      // accordion was hidden, so now display
      elementDiv.classList.remove('w3-hide');
      elementDiv.classList.add('w3-show');

      // change caret symbol to down-pointing small triangle
      elementCaret.innerHTML = '&#9662;';

      // make sure accordion button itself is inactive
      this.classList.remove('snu-active');
    } else {
      // accordion was displayed, so now hide
      elementDiv.classList.add('w3-hide');
      elementDiv.classList.remove('w3-show');

      // change caret symbol to right-pointing small triangle
      elementCaret.innerHTML = '&#9656;';

      // if a button in this accordion is active,
      // then also mark the accordion as active
      // (so it is easy to tell which accordion was last selected)
      let elementList =
        elementDiv.getElementsByClassName('snu-sidebar-button');
      for (let i = 0; i < elementList.length; i++) {
        let element = elementList[i];

        if (element.classList.contains('snu-active')) {
          this.classList.add('snu-active');
          break;
        }
      }
    }
  }

  // assign navigation sidebar open/close buttons
  document.getElementById('snu_close_button').onclick = snuClose;
  element = document.getElementById('snu_open_button');
  element.onclick = snuOpen;
  element.onclick(); // and open navigation sidebar

  // assign navigation sidebar buttons
  elementList = document.getElementsByClassName('snu-sidebar-button');
  for (let i = 0; i < elementList.length; i++) {
    let element = elementList[i];

    element.onclick = snuActivate;
  }

  // assign navigation sidebar accordions
  elementList = document.getElementsByClassName('snu-sidebar-accordion');
  for (let i = 0; i < elementList.length; i++) {
    let element = elementList[i];

    element.onclick = snuAccordion;
    element.onclick(); // and close accordion
  }

  // activate initial button
  elementList = document.getElementsByClassName('snu-sidebar-button');
  element = elementList[0];
  element.classList.add('snu-active');
  document.getElementById('snu_iframe').src = element.href;
}
