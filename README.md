# lower-carb

This repo contains recipes and tips for lower-carb eating.

It can be accessed through the following URL:

https://jeffbrecipes.gitlab.io/lower-carb/

## Implementation

The HTML, CSS, and JavaScript files are kept in the ``public`` directory
of this repo.  A YAML file named ``.gitlab-ci.yml`` file is kept at the top-level,
and is needed for GitLab to provide the URL for the website.

The HTML, CSS, and JavaScript files for the website were generated using
the Site Navigation Utility (SNU):

https://gitlab.com/jeffbstudio/snu

This script reads a YAML file (written by the website developer) which specifies
the buttons and accordion lists that make up the site's navigation sidebar,
and outputs an ``index.html`` file that implements the sidebar.
The website developer can edit this YAML file and re-run the SNU script at
any time.  The CSS and JavaScript for the site are also output by SNU when it
first generates ``index.html``, and the website developer can then edit and
customize these files, if desired.
